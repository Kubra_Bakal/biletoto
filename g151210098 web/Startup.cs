﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(g151210098_web.Startup))]
namespace g151210098_web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
